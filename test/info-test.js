var expect    = require("chai").expect;
var Info = require('../index').Info;

describe("Info", function() {

  it('Get info path without ext',function () {
    var filename = 'asdasda/asd';
    var info = new Info(filename);
    expect(info.filename).to.equal(filename+'.ifo');
  });

  it('Get info path',function () {
    var filename = 'asdasda/asd.ifo';
    var info = new Info(filename);
    expect(info.filename).to.equal(filename);
  });

  it('Get options',function () {
    var info = new Info('filename');
    expect(info.__get('asdasd')).to.equal(null);
    expect(info.__get('bookname')).to.equal('');
  });

  it('Call read file',function () {
    var info = new Info("./test/resources/star_vietnhat.ifo");
    var text = info.readFile();
    expect(info.__get('version')).to.equal('2.4.2');
    expect(info.__get('wordcount')).to.equal('21895');
    expect(info.__get('idxfilesize')).to.equal('414083');
    expect(info.__get('bookname')).to.equal('star_vietnhat');
  });

  it('getFilenameFor',function () {
    var info = new Info("./test/resources/star_vietnhat.ifo");
    expect(info.getFilenameFor('dict')).to.equal('./test/resources/star_vietnhat.dict');
  });

  it('getIdxFilename',function () {
    var info = new Info("./test/resources/star_vietnhat.ifo");
    expect(info.getIdxFilename()).to.equal('./test/resources/star_vietnhat.idx');
  });

  it('getDictFilename',function () {
    var info = new Info("./test/resources/star_vietnhat.ifo");
    expect(info.getDictFilename()).to.equal('./test/resources/star_vietnhat.dict');
  });
});
