var expect    = require("chai").expect;
var Index = require('../index').Index;
var Info = require('../index').Info;

describe("Index", function() {
  var info;
  beforeEach(function() {
    info = new Info("./test/resources/star_vietnhat.ifo");
  })

  it('getFilename',function () {
    var index = new Index(info);
    expect(index.getFilename()).to.equal('./test/resources/star_vietnhat.idx');
  });

  it('readFile',function () {
    var index = new Index(info);
    index.readFile();
  })
});
