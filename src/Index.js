var fs = require('fs'),
    _ = require('lodash'),
    bufferpack = require('bufferpack');

var Index = function (info) {
  this.info = info;
  this.word = {};
}

Index.prototype.getInfo = function () {
  return this.info;
};

Index.prototype.getFilename = function () {
  return this.info.getIdxFilename();
};

Index.prototype.getWordCount = function () {
  return _.keys(this.words).length;
};

Index.prototype.readFile = function () {
  var filename = this.getFilename();
  var data = fs.readFileSync(filename,'utf8');
  var fsize = data.length;
  console.log('fsize: '+fsize);
  console.log('idxfilesize: '+this.info.options.idxfilesize.val);
  var pos = 0;
  console.log('equals: '+this.info.options.idxfilesize.val == fsize);
  // while(pos < fsize){
  //   var chars = [];
  //   while(true){
  //     var x = bufferpack.unpack("@{"+pos+"}/Cch",data);
  //     console.log('x:');
  //     console.log(x);
  //     pos++;
  //     if(x['ch'] === 0){
  //       break;
  //     }
  //     chars.push(x['ch']);
  //   }
  // }
};

module.exports = Index;
