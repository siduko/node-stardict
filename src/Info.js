/**
 * Created by Thien on 6/2/2016.
 */
var fs = require('fs'),
    _ = require('lodash'),
    os = require('os');

var HEADER_STRING = "StarDict's dict ifo file";
var NEW_LINE_CHAR = os.platform() === 'win32'?"\r\n":"\n";

var Info =  function(filename){

    this.options = {
        'bookname': {'req': true, 'val': ''},
        'wordcount': {'req': true, 'val': ''},
        'idxfilesize': {'req': true, 'val': ''},
        'version': {'req': true, 'val': ''},
        'author': {'req': false, 'val': ''},
        'email': {'req': false, 'val': ''},
        'website': {'req': false, 'val': ''},
        'description': {'req': false, 'val': ''},
        'date': {'req': false, 'val': ''},
        'sametypesequence': {'req': true, 'val': ''},
    };

    if (filename.substring(filename.length-4,filename.length) !== '.ifo') {
        filename += '.ifo';
    }
    this.filename = filename;
    if(fs.existsSync(filename)){
      this.readFile();
    }
};

Info.prototype.validateHeader = function(header){
  return header == HEADER_STRING;
};

Info.prototype.readFile = function (cb) {
    _this = this;
    var text = fs.readFileSync(this.filename,'utf8');
    var lines = _.split(text,NEW_LINE_CHAR);
    _.forEach(lines,function (line) {
      var parts = _.split(line,'=');
      _this.__set(parts[0],parts[1]);
    })
    return text;
};

Info.prototype.__get = function (name) {
  if(this.options[name]){
    return this.options[name]['val'];
  }
  return null;
};

Info.prototype.__set = function (name,value) {
  if(this.options[name]){
    this.options[name]['val'] = value;
  }
};

Info.prototype.getFilename = function () {
  return this.filename;
};

Info.prototype.getInfo = function () {
  return this;
};

Info.prototype.validateRequired = function ()
{
    return _.findKey(this.options, function(o) { return o.req && _.empty(o.val)});
}

Info.prototype.getFilenameFor = function (ext) {
  var base = this.filename.substring(0,this.filename.lastIndexOf('.'));
  var path = base+'.'+ext;
  return fs.existsSync(path)?path:false;
};

Info.prototype.getIdxFilename = function()
{
    return this.getFilenameFor('idx');
}

Info.prototype.getDictFilename = function()
{
    return this.getFilenameFor('dict');
}

module.exports = Info;
